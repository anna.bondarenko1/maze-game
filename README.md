# maze-game

This is a simple maze game implemented in C# using Object-Oriented Programming (OOP) principles. The objective of the game is to navigate through a maze represented by a 2D grid and reach the goal position.
